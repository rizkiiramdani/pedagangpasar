<?php

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Auth extends CI_Controller {
        /**
         * Auth/Login Page for this controller session redirect dashboard .
        */
        function signin(){
            if (isset($_POST['submit'])){
                $user_name = $this->input->post('a');
                $user_password = hash("sha512", md5($this->input->post('b')));
                $cek = $this->db->query("SELECT * FROM ref_users where user_name='".$this->db->escape_str($user_name)."' AND user_password='".$this->db->escape_str($user_password)."'");
                $row = $cek->row_array();
                $total = $cek->num_rows();
                if ($total > 0){
                    $this->session->set_userdata(array(
                                    'user_nik'=>$row['user_nik'],
                                    'user_name'=>$row['user_name'],
                                    'usergroups_kodepos'=>$row['usergroups_kodepos'],
                                    'usergroups_id'=>$row['usergroups_id'],
                                    'level'=>'administrator'
                                ));
                    redirect('dashboard/home');
                }else{
                    $data['title'] = 'Aplikasi Pedagang Pasar &rsaquo; Log In';
                    $this->load->view('auth/login',$data);
                }
            }else{
                $data['title'] = 'Aplikasi Pedagang Pasar &rsaquo; Log In';
                $this->load->view('auth/login',$data);
            }
        }

        function logout(){
            $this->session->sess_destroy();
            redirect('auth/signin');
        }
    }


    