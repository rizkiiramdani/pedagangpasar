<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <td>
                Pasar Detail []
            </td>
        </h1>
        <ol class="breadcrumb">
            <li><a href="">Dashboard</a></li>
            <li><a href="">Pasar Detail []</a></li>
            <li class="active">View</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> Pasar Detail []</h3>
                        <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url(); ?>market/detail/$row[PASAR_KODEPOS]/create'>Tambahkan Data</a>
                    </div>
                    
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped" width="100%">
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Lantai</th>
                                    <th>Blok</th>
                                    <th>Nomor</th>
                                    <th>Nama Pedagang</th>
                                    <th>Panjang (M)</th>
                                    <th>Lebar (M)</th>
                                    <th>Retribusi Pasar (Rp)</th>
                                    <th>Retribusi Kebersihan (Rp)</th>
                                    <th>Kelompok Usaha</th>
                                    <th>Jenis Usaha</th>
                                    <th>Jenis Sampah</th>
                                    <th>Kepemilikan</th> 
                                    <th>Tipe Toko</th>
                                    <th>HPT</th>
                                    <th>HPT Balik Nama</th>
                                    <th>HER</th>
                                    <th>Her Masa Berlaku</th>
                                    <th>Keterangan</th>
                                    <th style='width:70px'>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $no = 1;
                                foreach ($record as $row){
                                echo "
                                    <tr>
                                        <td>$no</td>
                                        <td>$row[PASAR_ID] </td>
                                        <td>$row[BLOK] </td>
                                        <td>$row[NOMOR] </td>
                                        <td>$row[NAMA_PEDAGANG] </td>
                                        <td>$row[PANJANG_LAPAK_METER] </td>
                                        <td>$row[LEBAR_LAPAK_METER] </td>
                                        <td>Rp.$row[RETRIBUSI_HARIAN_LAPAK] </td>
                                        <td>Rp.$row[RETRIBUSI_HARIAN_SAMPAH] </td>
                                        <td>$row[KELOMPOKUSAHA_ID] NAME </td>
                                        <td>$row[JENISUSAHA_ID] NAME </td>
                                        <td>$row[JENISSAMPAH_ID] NAME </td>
                                        <td>$row[KEPEMILIKAN_ID] NAME </td>
                                        <td>$row[TIPETOKO_ID] NAME </td>
                                        <td>$row[HPT_ID] NAME </td>
                                        <td>$row[HPT_BALIKNAMA]</td>
                                        <td>$row[HER_ID] NAME </td>
                                        <td>$row[HER_TAHUNBERLAKU] </td>
                                        <td>$row[KETERANGAN] </td>
                                        <td><center>
                                            <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url()."market/detail/edit_market_detail/$row[PASAR_KODEPOS]'><span class='glyphicon glyphicon-edit'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url()."market/detail/delete_market_detail/$row[PASAR_KODEPOS]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                                        </center></td>
                                    </tr>";
                                $no++;
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>