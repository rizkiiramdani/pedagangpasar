<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Lantai
        </h1>
        <ol class="breadcrumb">
            <li><a href="">Dashboard</a></li>
            <li><a href="">Lantai</a></li>
            <li class="active">View</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a class='pull-right btn btn-primary btn-sm' href=''>Tambahkan Data</a>
                    </div>
                    
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped" width="100%">
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Lantai ID</th>
                                    <th>Name Lantai</th>
                                    <th style='width:70px'>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $no = 1;
                                foreach ($record as $row){
                                echo "
                                    <tr>
                                        <td>$no</td>
                                        <td>$row[LANTAI_ID]</td>
                                        <td>$row[LANTAI_NAME]</td>
                                        <td><center>
                                            <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url()."management/floor/edit_floor/$row[LANTAI_ID]'><span class='glyphicon glyphicon-edit'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url()."management/floor/delete_floor/$row[LANTAI_ID]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                                        </center></td>
                                    </tr>";
                                $no++;
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>